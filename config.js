const dotenv = require('dotenv');
const res = dotenv.config();
if (res.error) {
    console.error(res.error);
}
else {
    module.exports = {
        port: process.env.PORT
    };
}