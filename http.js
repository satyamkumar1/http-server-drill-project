const http = require('http');
const uuid = require('uuid');
const config = require('./config');


const server = http.createServer((req, res) => {
    req.url = req.url.toLowerCase();

    if (req.url == "/html") {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end("<!DOCTYPE html><html><head></head><body><h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1><p> - Martin Fowler</p></body></html>");

    }
    else if (req.url == "/json") {
       res.writeHead(200, { 'Content-Type': 'application/json' });
      
       const jsondata=JSON.stringify({
        "slideshow": {
            "author": "Yours Truly",
            "date": "date of publication",
            "slides": [
                {
                    "title": "Wake up to WonderWidgets!",
                    "type": "all"
                },
                {
                    "items": [
                        "Why <em>WonderWidgets</em> are great",
                        "Who <em>buys</em> WonderWidgets"
                    ],
                    "title": "Overview",
                    "type": "all"
                }
            ],
            "title": "Sample Slide Show"
        }
    })
        res.end(jsondata);
    }
    else if (req.url == "/uuid") {

        res.writeHead(200, { 'Content-Type': 'application/json' });
        const uuidNumber = uuid.v4()
        res.end(JSON.stringify({"uuid" :uuidNumber}));
    }
    else if (req.url.includes("/status")) {


        const resData = req.url.split('/')[2];
        

        if (http.STATUS_CODES[resData] && resData != 100 && resData.length==3) {
            res.writeHead(resData, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({
                "Status": resData
            }));
        }
        else {
            res.writeHead(400, { 'Content-Type': 'text/html' });
            res.end(JSON.stringify({ 'Status': 'invalid endpoint' }));
        }

    }
    else if (req.url.startsWith("/delay")) {

        const delaySeconds = req.url.split('/')[2];

        if (delaySeconds >= 0) {

            setTimeout(() => {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({"Response with 200 status code after": delaySeconds}));
            }, delaySeconds * 1000);
        }
        else {
            res.writeHead(404, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ "invalid time": delaySeconds }));
        }

    }
    else if (req.url == '/') {


        res.write(`<!DOCTYPE html><html>
        <head></head>
        <body style="text-align:center">
        
            <h1> Welcome </h1>
            <h1> Below the url link of endpoint </h1>
        
            <h2><a  href="/html"/>html</h2>
            <h2><a  href="/json"/>json</h2>
            <h2><a  href="/status/200"/>status</h2>
            <h2><a  href="/delay/2"/>delay</h2>

        
        
        </body>
        </html>`);
        res.end();
    }
    else {
        res.writeHead(404, { 'Content-Type': 'text/plain' });
        res.end("status : Sorry Invalid input");

    }


});

server.listen(config.port, () => {
    console.log("server is running on ");
});